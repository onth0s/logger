import { logT } from './index.js';

const obj = {
	name: 'Thomas',
	middleName: 'James',
	surname: 'Anderson',

	age: 26,

	married: false,

	// fuck: {
	// 	notSure: 'but',
	// 	im: 'confident'
	// },
	// fuck2: [1,2,3,4,5,6]
}

// logT([
// 	'Array.isArray([1, 2, 3])', Array.isArray([1, 2, 3])
// ], [
// 	'Array.isArray({ key: "value" })', Array.isArray({ key: "value" })
// ]);

// logT([
// 	'object:', { key1: 'value1', key2: 'value2' }
// ], [
// 	'array:', [1, 2, 3, 4, 5, 6, 7]
// ], [
// 	'here it breaks:',
// 	{ hello: { well: 'fuck' }, but: 'it\'s', not: 'so bad' }
// ], [
// 	'title',
// 	[1, 2, 3, 4, 5, 6]
// ], [
// 	`but what if the array
// 	contains actually an object?`,
// 	[1, {does: 'it', work: 'or not?'}, 2],
// 	{and: 'this?', it: 'almost', seems: 'to work'}
// ]);

logT([
	'object inside an object:',
	{a: {b: 'c'}, d: 1}
]);
