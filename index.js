import colors from 'colors';

const lineSymbol = '=';
let line = '';
for (let i = 0; i < 120; i++) {
	line += lineSymbol;
}

console.log("START " + line.bgGrey);

export const sp = (n) => {
	let spaces = '';
	for (let i = 0; i < n; i++) {
		spaces += ' ';
	}
	return spaces;
}

const numSpaces = 2;
export const parseObj = (obj) => {
	const objStr = JSON.stringify(obj);

	// extract start and end brackets → {}
	const _1 = objStr.split('');
	_1.pop(); _1.shift('');

	const _2 = _1.join('').split(',');

	let arr = [];
	if (_2.length > 1) _2.forEach(el => arr.push(el.split(':')));
	else arr = [_2.join('').split(':')];

	arr.forEach((el, i) => {
		el.forEach((el2, i2) => {
			// TODO HERE IT'S WHERE YOU CAN FIND THE KEY-VALUE PAIR
			if (i2 == 0) {
				const _1 = el[i2].split('');
				_1.splice(0, 1);
				_1.splice(_1.length - 1, 1);
				el[i2] = _1.join('');
			} else {
				arr[i][1] = arr[i][1].green;
			}
		});
		arr[i] = el.join(': ');
	});

	const res = [
		'{\n',

		sp(numSpaces),
		arr.join(',\n' + sp(numSpaces)),

		'\n}',
	];

	return res.join('');
}

const parseArr = (arr) => {
	const aL = arr.length;
	const num = 3;

	const dvd = aL / num;
	const mdl = aL % num;

	let arr2 = [];
	let arr3 = [];

	let counter = 0;
	for (let i = 0; i < Math.floor(dvd); i++) {
		for (let j = 0; j < num; j++) {
			arr2.push(String(arr[counter]).yellow);
			counter++;
		}
		arr3.push(arr2);
		arr2 = [];
	}
	for (let i = 0; i < mdl; i++) {
		arr2.push(String(arr[counter]).yellow);
		counter++;
	}
	if (mdl !== 0) arr3.push(arr2);

	let arr4 = [];
	arr3.forEach(el => {
		arr4.push(el.join(', '));
	});

	const _3 = [
		'[\n',

		sp(numSpaces),
		arr4.join(',\n' + sp(numSpaces)),

		'\n]',
	]

	return _3.join('');
}

export const logT = (...args) => {
	const symbol = '>>'.grey;
	const numSpaces = 3;

	args.forEach((el, i) => {
		const headerArr = el[0].toString().trim().split('\n');
		const header = [];

		headerArr.forEach((el, i) => {
			const temp = el.trim();
			if (i === 0) header.push(temp);
			else header.push('\n' + sp(5) + temp);
		});

		const headerOutput = i.toString().yellow + ' ' + symbol + ' ' + header.join('').brightBlue;
		console.log(headerOutput);

		// ↑ HEADER STUFF
		// ↓ 1st INDENT STUFF

		el.forEach((el2, i2) => {
			if (i2 > 0) {
				const subIndex = sp(numSpaces) + i + '.' + (i2 - 1);

				process.stdout.write(subIndex.yellow + ' ' + symbol + ' ');

				if (typeof (el2) === 'object' && !Array.isArray(el2)) {
					const _1 = parseObj(el2).split('\n');
					let arr = [];
					_1.forEach((el3, i3) => {
						if (i3 > 0) {
							arr.push('\n' + sp(numSpaces) + el3)
						} else {
							arr.push(el3);
						}
					})

					console.log(arr.join(''));
				} else if (Array.isArray(el2)) {
					const _1 = parseArr(el2).split('\n');
					let arr = [];
					_1.forEach((el3, i3) => {
						if (i3 > 0) {
							arr.push('\n' + sp(numSpaces) + el3)
						} else {
							arr.push(el3);
						}
					}); console.log(arr.join(''));
				} else console.log(el2);
			}
		});

		console.log('');
	});
}
