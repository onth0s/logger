import { logT } from './index.js';

// logT([
// 	'this is a test',
// 	'yeah, yes it is',
// 	12312312312,
// 	99 + ' luftballons',
// 	`what about ${1 + 2} literals?`,
// 	[1, 2, 3, 4, 5, 6, 7, 8],
// 	{ test: 'really long string asd fas df asdf asf asdf as df ', test2: 2, test3: 3 }
// ])
// logT([
// 	`this is a test
// 	that works like a charm`,
// 	100
// ], [
// 	'second',
// 	'nooooo',
// 	`i failed
// 	  you mom`.trap
// ])



const obj = {
	test: 'value',
	second: 222,
	test2: {
		oh: 'shit',
		oh1: 'shit',
		oh2: 'shit',
		oh3: 'shit',
		oh4: 'shit',
		oh5: 'shit',
		oh6: 'shit',
	}
}

const s_ = JSON.stringify(obj);

let s = s_;

const s2 = s.split('');
s2.pop();
s2.shift();

const s2_1 = s2.join('');
const s2_2 = s2_1.split(',');

for (let i = 0; i < s2_2.length; i++) {
	let c = ',';
	let ln = i == s2_2.length - 1 ? '' : '\n';
	let spacing = ln + sp(3);
	s2_2[i] += c + spacing;
}

const s2_3 = s2_2.join('');
const s2_4 = s2_3.replace(/:/g, ': ');

const s3 = '{\n' + sp(3) + s2_4 + '\n}';

console.log(
	s3
);

// TODO can't use this 'cause the wrong indentation of nested objects 'n shit, ironic
// logT([
// 	's2_1: ',
// 	s2_1
// ], [
// 	's3',
// 	s3
// ]);
